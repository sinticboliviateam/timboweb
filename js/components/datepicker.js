Vue.component('datepicker', 
{
    template: `<input type="text" />`,
    props: ['value', 'class', 'placeholder', 'format'],
    mounted: function () 
    {
        var self = this;
        $(self.$el)                    
            .datepicker({ minDate: this.min_date, value: this.value, format: this.format || 'yyyy-mm-dd' }) // init datepicker
            .trigger('change')                    
            .on('change', function () { // emit event on change.
                self.$emit('input', this.value);
            })
    },
    watch: {
        value: function (value) {
            $(this.$el).val(value);
        }
    },
    destroyed: function () {
        $(this.$el).datepicker('destroy');
    }
});