let basePath = '';
if( window.location.hostname == 'localhost' )
{
	basePath = '/timbo-web';
}
const routes = {
	'^/$': {com: 'app-content', public: false},
	'/login/?$': 'com-login',
	'/registro/?$': 'com-register',
	'^/categories/(\\d+)/?$': {com: 'com-category', public: true},
	'^/groups/(\\d+)/?$': {com: 'com-group', public: true},
	//'/campeonatos/(\\d+)/?$': {com: 'com-campeonato-single', public: false},
};
