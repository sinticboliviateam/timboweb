let ComSearch = {
	template: `
	<div id="com-search">
		<div class="form-group">
			<input type="text" v-model="keyword" class="form-control" /> 
		</div>
	</div>
	`,
	data: function()
	{
		return {
			keyword: ''
		};
	},
	methods: 
	{
	},
	created: function()
	{
	},
	mounted: function()
	{
	}
};
Vue.component('search', ComSearch);
