let VLink = {
	template: `<a v-bind:href="basePath + href" v-bind:class="{ active: isActive }" v-on:click="go($event)"><slot></slot></a>`,
	props: {
		href: {type: String, required: true},
		class: {type: String, required: false},
	},
	computed: 
	{
		isActive()
		{
			return this.href == this.$root.currentRoute;
		}
	},
	methods: 
	{
		go: function(e)
		{
			console.log('go()');
			e.preventDefault();
			Router.Open(this.href);
		}
	}
};
Vue.component('v-link', VLink);