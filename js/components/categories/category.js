let ComCategory = {
	template: `
	<div>
		<div v-if="category">
			<h1>{{ category.name }}</h1>
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 col-sm-4 col-md-3 col-lg-3" v-for="group in groups">
						<div class="music-group" v-on:click="openGroup(group, $event);">
							<figure class="image">
								<img v-bind:src="group.thumbnail_url" alt="" class="img-fluid" />
							</figure>
							<h4 class="title">{{ group.title }}</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	`,
	props: ['args'],
	data: function()
	{
		return {
			id: null,
			category: null,
			groups: [],
		};
	},
	methods: 
	{
		getCategory: function()
		{
			this.$root.api.GetCategory(this.id)
				.then((res) => 
				{
					this.category = res.data;
				})
				.catch( () => 
				{
				});
		},
		getGroups: function()
		{
			this.$root.api.GetCategoryGroups(this.id)
				.then((res) => 
				{
					this.groups = res.data;
				})
				.catch( () => 
				{
				});
		},
		openGroup: function(group, evt)
		{
			Router.Open('/groups/' + group.content_id);
		}
	},
	mounted: function()
	{
	},
	created: function()
	{
		console.log('com-category -> created');
		this.id = this.args[0];
		this.getCategory();
		this.getGroups();
	}
};
Vue.component('com-category', ComCategory);
