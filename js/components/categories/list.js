let ComCategoriesList = {
	template: `
	<ul class="list">
		<li v-for="cat in categories">
			<v-link v-bind:href="'/categories/' + cat.section_id" class="">{{ cat.name }}</v-link>
		</li>
	</ul>
	`,
	data: function()
	{
		return {
			categories: []
		};
	},
	methods: 
	{
		getCategories: function()
		{
			this.$root.api.GetCategories()
				.then((res) => 
				{
					console.log(res);
					this.categories = res.data;
				})
				.catch( (e) => 
				{
					
				});
		}
	},
	mounted: function()
	{
		
	},
	created: function()
	{
		this.getCategories()
	}
};
Vue.component('categories-list', ComCategoriesList);
