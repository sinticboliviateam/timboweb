let SBPlayer = {
	template: `
	<div id="sbplayer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-2">
					<div class="text-center">
						<figure>
							<img v-if="currentTrack && currentTrack.cover" v-bind:src="currentTrack.cover" alt="" class="img-fluid" />
						</figure>
					</div>
				</div>
				<div class="col-sm-7">
					<div class="track-info" v-if="currentTrack">
						<div class="track-name">
							{{ currentTrack.name }} - {{ currentTrack.album }}
						</div>
						<div class="group-name">
							{{ currentTrack.group }}
						</div>
					</div>
					<div class="sb-player-controls">
						<div class="container-fluid">
							<div class="row justify-content-center">
								<div class="col-6 text-center">
									<a href="javascript:;" class="ctrl"><i class="fa fa-step-backward"></i></a>
									<a href="javascript:;" class="ctrl" v-if="!playing" v-on:click="play(0, $event);">
										<i class="fa fa-play"></i>
									</a>
									<a href="javascript:;" class="ctrl" v-else v-on:click="pause($event);">
										<i class="fa fa-pause"></i>
									</a>
									<a href="javascript:;" class="ctrl" v-on:click="next($event);">
										<i class="fa fa-step-forward"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="sb-play-progress-wrap">
						<span class="elapsed">{{ elapsed }}</span>
						<span class="total-time">{{ totalTime }}</span>
						<div class="sb-play-progress">
							<div class="progress">
								<div class="progress-bar" v-bind:style="{width: progress + '%'}"></div>
								<span class="handler" v-bind:style="{left: progress + '%'}">&nbsp;</span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="sb-player-volume">
						<div class="container-fluid">
							<div class="row">
								<div class="col">
									<div class="ctrl"><i class="fa fa-volume-up"></i></div>
								</div>
								<div class="col">
									<div class="sb-volume-progress">
										<div class="progress">
											<div class="progress-bar"></div>
											<span class="handler">&nbsp;</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<audio id="sbplayer-audio" ref="player"></audio>
	</div>
	`,
	props: ['args'],
	data: function()
	{
		return {
			player: null,
			currentIndex: -1,
			playlist: [],
			playing: false,
			paused: false,
			progress: 0,
			elapsed: '00:00',
			totalTime: '00:00',
		};
	},
	computed:
	{
		currentTrack: function()
		{
			let track = this.playlist[this.currentIndex];
			if( !track )
				return null;
			return track;
		}
	},
	methods: 
	{
		setPlayList: function(items)
		{
			console.log('SBPlayer.setPlayList()', items);
			this.playlist = items;
			this.play(0);
		},
		pause: function()
		{
			this.player.pause();
			this.playing = false;
			this.paused = true;
		},
		play: function(index, $evt)
		{
			console.log('index', index);
			if( this.paused )
			{
				this.paused = false;
				this.playing = true;
				this.player.play();
				return true;
			}
			if( !this.playlist[parseInt(index)] )
				return false;
				
			this.currentIndex = parseInt(index);
			
			
			let track = this.playlist[this.currentIndex];
			console.log(track);
			this.player.setAttribute('src', track.stream_url);
			this.player.load();
			
			
		},
		_playerCanPlay: function()
		{
			//console.log('canplay', 'duration', this.player.duration);
			this.totalTime = this._trackTime(this.player.duration);
			this.player.play();
			this.playing = true;
		},
		next: function($evt)
		{
			this.play(this.currentIndex + 1);
		},
		_playerTimeChanged: function()
		{
			//console.log(this.player.currentTime, this.player.duration);
			this.progress = (this.player.currentTime * 100) / this.player.duration;
			this.elapsed = this._trackTime(this.player.currentTime);
		},
		_playerTrackEnded: function()
		{
			console.log('ended');
			let nextIndex = this.curreIndex + 1;
			this.play(nextIndex);
		},
		_playerError: function()
		{
			console.log('SBPlayer ERROR', arguments);
		},
		_trackTime: function(seconds) 
		{
			var min = 0;
			var sec = Math.floor(seconds);
			var time = 0;
			min = Math.floor(sec / 60);
			min = min >= 10 ? min : '0' + min;
			sec = Math.floor(sec % 60);
			sec = sec >= 10 ? sec : '0' + sec;
			time = min + ':' + sec;
			return time;
		}
	},
	mounted: function()
	{
		//console.log(this.$refs);
		this.player = this.$refs.player;
		this.player.addEventListener('canplay', () => {this._playerCanPlay();}, false);
		this.player.addEventListener('timeupdate', () => {this._playerTimeChanged();}, false);
		this.player.addEventListener('ended', () => {this._playerTrackEnded();}, false);
		this.player.addEventListener('error', () => {this._playerError();}, false);
	},
	created: function()
	{

	}
};
Vue.component('sbplayer', SBPlayer);
