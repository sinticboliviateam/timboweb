let app = new Vue({
	el: '#timbo-app', 
	data: 
	{
		showSidebar: true,
		contentClass: 'col-xs-12 col-sm-9 col-md-9 col-lg-9',
		currentRoute: Router.GetCurrentRoute(),
		currentCom: (function()
		{
			let component = Router.GetCom(Router.GetCurrentRoute());
			return typeof component == 'object' ? component.com : component;
		})(),
		comData: Router.GetData(),
		routeData: Router.routeData,
		api: null,
		toast: {
			message: null,
			success: false,
			danger: false
		}
	},
	computed: 
	{
		/*
		ViewComponent: function()
		{
			const matchingView = routes[this.currentRoute];
			return matchingView ? require('./pages/' + matchingView + '.vue') : require('./pages/404.vue');
		},
		*/
		com_current: function()
		{
			this.CheckSession();
			return this.currentCom;
		},
		com_args: function()
		{
			//let data = Router.ParseURL(this.currentRoute);
			return this.routeData;
		},
		com_key: function()
		{
			return this.currentCom + Date.now();
		}
	},
	methods: 
	{
		CheckSession: function()
		{
			if( !sessionStorage.getItem('jwt') )
			{
				this.showSidebar = false;
				this.contentClass = 'col';
				document.body.classList.add('page-login');
			}
			else
			{
				document.body.classList.remove('page-login');
			}
		},
		Logout: function()
		{
			sessionStorage.removeItem('jwt');
			sessionStorage.removeItem('user');
			Router.Open('/login');
		},
		ShowToast: function(message, type)
		{
			this.toast.message = message;
			if( type == 'success' )
				this.toast.success = true;
			if( type == 'danger' )
				this.toast.danger = true;
			if( type == 'info' )
				this.toast.info = true;
			setTimeout(() => {
				this.toast.message = null;
			}, 10000);
		},
		CloseToast: function()
		{
			this.toast.message = null;
		}
	},
	created: function()
	{
		this.api = new ApiTimbo();
	},
	mounted: function()
	{
		
	},
	updated: function()
	{
		
	}
	/*,
	render: function(h)
	{
		return h(this.ViewComponent);
	}*/
});
