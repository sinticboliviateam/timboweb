class ApiTimbo extends Api
{
	constructor()
	{
		super();
		this.apiBase = 'https://timbo.hexadatos.net/api';
	}
	GetCategories(parent)
	{
		let endpoint = '/timbo/categories';
		return this.Get(endpoint);
	}
	GetCategory(id)
	{
		let endpoint = '/timbo/categories/' + id;
		
		return this.Get(endpoint);
	}
	GetCategoryGroups(id)
	{
		let endpoint = '/timbo/categories/' + id + '/groups';
		
		return this.Get(endpoint);
	}
	GetGroup(id)
	{
		let endpoint = '/timbo/groups/' + id;
		return this.Get(endpoint);
	}
	GetAlbums(groupId)
	{
		let endpoint = '/timbo/groups/' + groupId + '/albums';
		return this.Get(endpoint);
	}
}
