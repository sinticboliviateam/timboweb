class Api
{
	constructor()
	{
		console.log('Api::contructor');
		this.apiBase = null;
	}
	GetToken(username, pwd)
	{
		let endpoint = this.apiBase + '/api/v1.0.0/users/get-token/'; //Router.Link('/api/v1.0.0/users/get-token/');
		let ops = {
			method: 'POST',
			body: JSON.stringify({username: username, password: pwd}),
			mode: 'cors',
			cache: 'default'
		};
		return new Promise( (resolve, reject) => 
		{
			fetch(endpoint, ops)
				.then( (r) => 
				{
					//if( r.status != 200 )
					if( !r.ok )
					{
						throw r;
					}
					return r.json();
				})
				.then( (data) => 
				{
					resolve(data);
				})
				.catch( (e) => 
				{
					e.json().then( (data) => 
					{
						reject(data);
					});
				});
		});
	}
	Request(endpoint, method, data, $headers)
	{
		let headers = {
			'Content-type': 'application/json'
		};
		if( sessionStorage.getItem('jwt') )
		{
			headers['Authorization'] = 'Bearer ' + sessionStorage.getItem('jwt');
		}
		if( typeof $headers == 'object' )
		{
			for(let key in $headers)
			{
				headers[key] = $headers[key];
			}
		}
		let ops = {
			method: method,
			body: JSON.stringify(data),
			mode: 'cors',
			cache: 'default',
			headers: headers
		};
		return new Promise( (resolve, reject) => 
		{
			fetch(endpoint, ops)
			.then( (r) => 
			{
				//if( r.status != 200 )
				if( !r.ok )
				{
					throw r;
				}
				return r.json();
			})
			.then( (data) => 
			{
				resolve(data);
			})
			.catch( (e) => 
			{
				if( !e || !e.json )
				{
					reject({error: 'Error desconocido'});
					return;
				}
				e.json().then( (data) => 
				{
					reject(data);
				});
			});
		});
	}
	Get(endpoint)
	{
		return this.Request(this.apiBase + endpoint, 'GET');
	}
	Post(endpoint, data, headers)
	{
		return this.Request(this.apiBase + endpoint, 'POST', data, headers);
	}
	Put(endpoint, data, headers)
	{
		return this.Request(this.apiBase + endpoint, 'PUT', data, headers);
	}
	Delete(endpoint)
	{
		return this.Request(this.apiBase + endpoint, 'DELETE');
	}
}
