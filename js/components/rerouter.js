let Router = 
{
	routeData: [],
	ParseURL: function(url) 
	{
	    var a = document.createElement('a');
	    a.href = url;
	    return {
	        source: url,
	        protocol: a.protocol.replace(':', ''),
	        host: a.hostname,
	        port: a.port,
	        query: a.search,
	        params: (function () {
	            var ret = {},
	                seg = a.search.replace(/^\?/, '').split('&'),
	                len = seg.length,
	                i = 0,
	                s;
	            for (; i < len; i++) {
	                if (!seg[i]) {
	                    continue;
	                }
	                s = seg[i].split('=');
	                ret[s[0]] = s[1];
	            }
	            return ret;
	        })(),
	        file: (a.pathname.match(/\/([^\/?#]+)$/i) || [, ''])[1],
	        hash: a.hash.replace('#', ''),
	        path: a.pathname.replace(/^([^\/])/, '/$1'),
	        relative: (a.href.match(/tps?:\/\/[^\/]+(.+)/) || [, ''])[1],
	        segments: a.pathname.replace(/^\//, '').split('/')
	    };
	},
	GetCurrentRoute: function()
	{
		let route = window.location.pathname.replace(basePath, '');
		console.log('CurrentRoute', route);
		return route;
	},
	GetCom: function(route)
	{
		let component = null;
		let current_route = route;
		for(let $route of Object.keys(routes))
		{
			let $pattern 	= $route.replace('.*', '[a-zA-Z0-9\-.]+');
			let regex 		= new RegExp($pattern, 'g');
			let res 		= regex.exec(current_route);
			if( res )
			{
				component = routes[$route];
				res.splice(0, 1);
				Router.routeData = res;
				break;
			}
		}
		if(  !Router.ValidateAccess(component) )
		{
			route 		= Router.Link('/login');
			component	= 'com-login';
		}
		//console.log(component);
		//console.log(Router.ParseURL(window.location));
		return component || 'app-content';
	},
	GetData: function(url)
	{
		let data = Router.ParseURL(url || Router.GetCurrentRoute());
		
		return data;
	},
	Open: function(path)
	{
		let route			= basePath + path;
		let component		= Router.GetCom(path);
		
		if(  !Router.ValidateAccess(component) )
		{
			route = Router.Link('/login');
			component = 'com-login';
		}
		console.log('Open', component, path, Router.routeData, app.currentCom);
		app.currentRoute 	= path;
		app.currentCom		= null; //to avoid state with same component
		app.currentCom		= typeof component == 'object' ? component.com : component;
		app.comData			= Router.GetData();
		app.routeData		= Router.routeData;
		window.history.pushState(null, app.currentCom, route);
	},
	Link: function(url)
	{
		return basePath + url;
	},
	ValidateAccess: function(component)
	{
		let res = true;
		if( component && typeof component == 'object' && typeof component.public != 'undefined' )
		{
			if( !component.public )
			{
				let jwt = sessionStorage.getItem('jwt');
				if( !jwt )
				{
					res = false;
				}
			}
		}
		return res;
	}
}
window.addEventListener('popstate', () => 
{
	Router.Open(Router.GetCurrentRoute());
	//app.currentRoute = Router.GetCurrentRoute();
});
