let ComGroup = {
	template: `
	<div>
		<div v-if="group">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 col-sm-8 col-lg-8">
						<h1>{{ group.title }}</h1>
						<div class="banner" v-if="group.images && group.images.length > 0">
							<img v-bind:src="group.images[0].url" alt="" class="img-fluid" />
						</div>
						<div class="card">
							<div class="card-header"><h5>Biografía</h5></div>
							<div class="card-body" v-html="group.content"></div>
						</div>
					</div>
					<div class="col-12 col-sm-4 col-lg-4">
						<div class="card">
							<div class="card-header"><h5>Discografia</h5></div>
							<div class="card-body">
								<div class="container-fluid">
									<div class="row">
										<div class="col-12 col-sm-6" v-for="album in albums">
											<div class="group-album">
												<figure>
													<img v-if="album.cover" v-bind:src="album.cover.thumbnailUrl" alt="" class="img-fluid" />
												</figure>
												<div class="name">{{ album.name }}</div>
												<a href="javascript:;" class="play-album" v-on:click="playAlbum(album, $event);">
													<i class="fa fa-play"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<div>
	</div>
	`,
	props: ['args'],
	data: function()
	{
		return {
			group: null,
			albums: [],
		};
	},
	methods: 
	{
		getGroup: function(id)
		{
			this.$root.api.GetGroup(id)
				.then( (res) => 
				{
					this.group = res.data;
				})
				.catch( (e) => 
				{
				});
		},
		getAlbums: function(id)
		{
			this.$root.api.GetAlbums(id).then( (res) => 
			{
				this.albums = res.data;
			})
			.catch( (e) => 
			{
			});
		},
		playAlbum: function(album, evt)
		{
			this.$root.$refs.$sbplayer.setPlayList(album.tracks);
		}
	},
	mounted: function()
	{
	},
	created: function()
	{
		this.getGroup(this.args[0]);
		this.getAlbums(this.args[0]);
	}
};
Vue.component('com-group', ComGroup);
